package nc.unc.importparcoursup.dao.admisDAO.repository;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AdmisPageableRepository extends PagingAndSortingRepository<Admis, Long> {
    Page<Admis> findAllAdmisByAdmisHistory(AdmisHistory admisHistory, Pageable pageable);
}
