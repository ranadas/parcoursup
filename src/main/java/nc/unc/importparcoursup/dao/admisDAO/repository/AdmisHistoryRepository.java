package nc.unc.importparcoursup.dao.admisDAO.repository;

import nc.unc.importparcoursup.dao.IHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.model.UAI;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface AdmisHistoryRepository extends CrudRepository<AdmisHistory, Long>, IHistoryRepository {
    AdmisHistory findFirstByFileCheckSum(String checkSum);

    AdmisHistory findTopByUaiOrderByJobExecutionDateDesc(UAI uai);

    AdmisHistory findTopByOrderByJobExecutionDateDesc();
}
