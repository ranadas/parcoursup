package nc.unc.importparcoursup.dao.admisDAO.repository;

import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.AdmisRejet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AdmisRejetRepository extends CrudRepository<AdmisRejet, Long> {
    @Query("SELECT DISTINCT ar " +
            "FROM AdmisRejet ar " +
            "WHERE ar.admis.admisHistory =:history")
    List<AdmisRejet> findAdmisRejetByAdmisHistory(@Param("history") AdmisHistory history);
}
