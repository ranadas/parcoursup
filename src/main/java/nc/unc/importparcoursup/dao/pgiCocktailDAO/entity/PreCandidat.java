package nc.unc.importparcoursup.dao.pgiCocktailDAO.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = PreCandidat.TABLE_NAME)
public class PreCandidat {
    public final static String TABLE_NAME = "GARNUCHE.PRE_CANDIDAT";

    @OneToOne(mappedBy = "preCandidat", cascade = CascadeType.ALL)
    private PreCandidature preCandidature;

    @Id
    private long CAND_KEY;
    private long CAND_NUMERO;
    private long ETUD_NUMERO;
    private String CAND_BEA;
    private String CAND_CIVILITE;
    private String CAND_NOM;
    private String CAND_PRENOM;
    private String CAND_PRENOM2;
    private Date CAND_DATE_NAIS;
    private String CAND_COMNAIS;
    private String DPTG_CODE_NAIS;
    private String PAYS_CODE_NAIS;
    private String NAT_ORDRE;
    private String CAND_ADR1_PARENT;
    private String CAND_ADR2_PARENT;
    private String CAND_CP_PARENT;
    private String CAND_VILLE_PARENT;
    private String CAND_PAYS_PARENT;
    private String CAND_TEL_PARENT;
    private String CAND_ADR1_SCOL;
    private String CAND_ADR2_SCOL;
    private String CAND_CP_SCOL;

    // vérifier dans commune.
    private String CAND_VILLE_SCOL;
    private String CAND_PAYS_SCOL;
    private String CAND_TEL_SCOL;
    private String CAND_PORT_SCOL;
    private String CAND_EMAIL_SCOL;
    private long CAND_ANBAC;

    // Vérification dans bac.bac_code
    private String BAC_CODE;

    private String ETAB_CODE_BAC;
    private String ETAB_LIBELLE_BAC;
    private String CAND_VILLE_BAC;
    private String DPTG_ETAB_BAC;
    private String ACAD_ETAB_BAC;
    private String PAYS_ETAB_BAC;

    // toujours rne unc 9830445S ?
    private String C_RNE;

    private String HIST_ENS_DER_ETAB;

    // rne du fichier (on vérifie qu'il existe dans la table rne
    private String ETAB_CODE_DER_ETAB;

    private String HIST_LIBELLE_DER_ETAB;
    private String HIST_VILLE_DER_ETAB;
    private String DPTG_CODE_DER_ETAB;
    private String PAYS_CODE_DER_ETAB;

    public PreCandidat() {

    }

    public PreCandidat (long id){
        this.CAND_KEY = id;
    }

    public PreCandidature getPreCandidature() {
        return preCandidature;
    }

    public void setPreCandidature(PreCandidature preCandidature) {
        this.preCandidature = preCandidature;
    }

    public long getCAND_KEY() {
        return CAND_KEY;
    }

    public void setCAND_KEY(long cAN_KEY) {
        CAND_KEY = cAN_KEY;
    }

    public long getCAND_NUMERO() {
        return CAND_NUMERO;
    }

    public void setCAND_NUMERO(long cAND_NUMERO) {
        CAND_NUMERO = cAND_NUMERO;
    }

    public long getETUD_NUMERO() {
        return ETUD_NUMERO;
    }

    public void setETUD_NUMERO(long eTUD_NUMERO) {
        ETUD_NUMERO = eTUD_NUMERO;
    }

    public String getCAND_BEA() {
        return CAND_BEA;
    }

    public void setCAND_BEA(String cAND_BEA) {
        CAND_BEA = cAND_BEA;
    }

    public String getCAND_CIVILITE() {
        return CAND_CIVILITE;
    }

    public void setCAND_CIVILITE(String cAND_CIVILITE) {
        CAND_CIVILITE = cAND_CIVILITE;
    }

    public String getCAND_NOM() {
        return CAND_NOM;
    }

    public void setCAND_NOM(String cAND_NOM) {
        CAND_NOM = cAND_NOM;
    }

    public String getCAND_PRENOM() {
        return CAND_PRENOM;
    }

    public void setCAND_PRENOM(String cAND_PRENOM) {
        CAND_PRENOM = cAND_PRENOM;
    }

    public String getCAND_PRENOM2() {
        return CAND_PRENOM2;
    }

    public void setCAND_PRENOM2(String cAND_PRENOM2) {
        CAND_PRENOM2 = cAND_PRENOM2;
    }

    public Date getCAND_DATE_NAIS() {
        return CAND_DATE_NAIS;
    }

    public void setCAND_DATE_NAIS(Date cAND_DATE_NAIS) {
        CAND_DATE_NAIS = cAND_DATE_NAIS;
    }

    public String getCAND_COMNAIS() {
        return CAND_COMNAIS;
    }

    public void setCAND_COMNAIS(String cAND_COMNAIS) {
        CAND_COMNAIS = cAND_COMNAIS;
    }

    public String getDPTG_CODE_NAIS() {
        return DPTG_CODE_NAIS;
    }

    public void setDPTG_CODE_NAIS(String dPTG_CODE_NAIS) {
        DPTG_CODE_NAIS = dPTG_CODE_NAIS;
    }

    public String getPAYS_CODE_NAIS() {
        return PAYS_CODE_NAIS;
    }

    public void setPAYS_CODE_NAIS(String pAYS_CODE_NAIS) {
        PAYS_CODE_NAIS = pAYS_CODE_NAIS;
    }

    public String getNAT_ORDRE() {
        return NAT_ORDRE;
    }

    public void setNAT_ORDRE(String nAT_ORDRE) {
        NAT_ORDRE = nAT_ORDRE;
    }

    public String getCAND_ADR1_PARENT() {
        return CAND_ADR1_PARENT;
    }

    public void setCAND_ADR1_PARENT(String cAND_ADR1_PARENT) {
        CAND_ADR1_PARENT = cAND_ADR1_PARENT;
    }

    public String getCAND_ADR2_PARENT() {
        return CAND_ADR2_PARENT;
    }

    public void setCAND_ADR2_PARENT(String cAND_ADR2_PARENT) {
        CAND_ADR2_PARENT = cAND_ADR2_PARENT;
    }

    public String getCAND_CP_PARENT() {
        return CAND_CP_PARENT;
    }

    public void setCAND_CP_PARENT(String cAND_CP_PARENT) {
        CAND_CP_PARENT = cAND_CP_PARENT;
    }

    public String getCAND_VILLE_PARENT() {
        return CAND_VILLE_PARENT;
    }

    public void setCAND_VILLE_PARENT(String cAND_VILLE_PARENT) {
        CAND_VILLE_PARENT = cAND_VILLE_PARENT;
    }

    public String getCAND_PAYS_PARENT() {
        return CAND_PAYS_PARENT;
    }

    public void setCAND_PAYS_PARENT(String cAND_PAYS_PARENT) {
        CAND_PAYS_PARENT = cAND_PAYS_PARENT;
    }

    public String getCAND_TEL_PARENT() {
        return CAND_TEL_PARENT;
    }

    public void setCAND_TEL_PARENT(String cAND_TEL_PARENT) {
        CAND_TEL_PARENT = cAND_TEL_PARENT;
    }

    public String getCAND_ADR1_SCOL() {
        return CAND_ADR1_SCOL;
    }

    public void setCAND_ADR1_SCOL(String cAND_ADR1_SCOL) {
        CAND_ADR1_SCOL = cAND_ADR1_SCOL;
    }

    public String getCAND_ADR2_SCOL() {
        return CAND_ADR2_SCOL;
    }

    public void setCAND_ADR2_SCOL(String cAND_ADR2_SCOL) {
        CAND_ADR2_SCOL = cAND_ADR2_SCOL;
    }

    public String getCAND_CP_SCOL() {
        return CAND_CP_SCOL;
    }

    public void setCAND_CP_SCOL(String cAND_CP_SCOL) {
        CAND_CP_SCOL = cAND_CP_SCOL;
    }

    public String getCAND_VILLE_SCOL() {
        return CAND_VILLE_SCOL;
    }

    public void setCAND_VILLE_SCOL(String cAND_VILLE_SCOL) {
        CAND_VILLE_SCOL = cAND_VILLE_SCOL;
    }

    public String getCAND_PAYS_SCOL() {
        return CAND_PAYS_SCOL;
    }

    public void setCAND_PAYS_SCOL(String cAND_PAYS_SCOL) {
        CAND_PAYS_SCOL = cAND_PAYS_SCOL;
    }

    public String getCAND_TEL_SCOL() {
        return CAND_TEL_SCOL;
    }

    public void setCAND_TEL_SCOL(String cAND_TEL_SCOL) {
        CAND_TEL_SCOL = cAND_TEL_SCOL;
    }

    public String getCAND_PORT_SCOL() {
        return CAND_PORT_SCOL;
    }

    public void setCAND_PORT_SCOL(String cAND_PORT_SCOL) {
        CAND_PORT_SCOL = cAND_PORT_SCOL;
    }

    public String getCAND_EMAIL_SCOL() {
        return CAND_EMAIL_SCOL;
    }

    public void setCAND_EMAIL_SCOL(String cAND_EMAIL_SCOL) {
        CAND_EMAIL_SCOL = cAND_EMAIL_SCOL;
    }

    public long getCAND_ANBAC() {
        return CAND_ANBAC;
    }

    public void setCAND_ANBAC(long cAND_ANBAC) {
        CAND_ANBAC = cAND_ANBAC;
    }

    public String getBAC_CODE() {
        return BAC_CODE;
    }

    public void setBAC_CODE(String bAC_CODE) {
        BAC_CODE = bAC_CODE;
    }

    public String getETAB_CODE_BAC() {
        return ETAB_CODE_BAC;
    }

    public void setETAB_CODE_BAC(String eTAB_CODE_BAC) {
        ETAB_CODE_BAC = eTAB_CODE_BAC;
    }

    public String getETAB_LIBELLE_BAC() {
        return ETAB_LIBELLE_BAC;
    }

    public void setETAB_LIBELLE_BAC(String eTAB_LIBELLE_BAC) {
        ETAB_LIBELLE_BAC = eTAB_LIBELLE_BAC;
    }

    public String getCAND_VILLE_BAC() {
        return CAND_VILLE_BAC;
    }

    public void setCAND_VILLE_BAC(String cAND_VILLE_BAC) {
        CAND_VILLE_BAC = cAND_VILLE_BAC;
    }

    public String getDPTG_ETAB_BAC() {
        return DPTG_ETAB_BAC;
    }

    public void setDPTG_ETAB_BAC(String dPTG_ETAB_BAC) {
        DPTG_ETAB_BAC = dPTG_ETAB_BAC;
    }

    public String getACAD_ETAB_BAC() {
        return ACAD_ETAB_BAC;
    }

    public void setACAD_ETAB_BAC(String aCAD_ETAB_BAC) {
        ACAD_ETAB_BAC = aCAD_ETAB_BAC;
    }

    public String getPAYS_ETAB_BAC() {
        return PAYS_ETAB_BAC;
    }

    public void setPAYS_ETAB_BAC(String pAYS_ETAB_BAC) {
        PAYS_ETAB_BAC = pAYS_ETAB_BAC;
    }

    public String getC_RNE() {
        return C_RNE;
    }

    public void setC_RNE(String c_RNE) {
        C_RNE = c_RNE;
    }

    public String getHIST_ENS_DER_ETAB() {
        return HIST_ENS_DER_ETAB;
    }

    public void setHIST_ENS_DER_ETAB(String hIST_ENS_DER_ETAB) {
        HIST_ENS_DER_ETAB = hIST_ENS_DER_ETAB;
    }

    public String getETAB_CODE_DER_ETAB() {
        return ETAB_CODE_DER_ETAB;
    }

    public void setETAB_CODE_DER_ETAB(String eTAB_CODE_DER_ETAB) {
        ETAB_CODE_DER_ETAB = eTAB_CODE_DER_ETAB;
    }

    public String getHIST_LIBELLE_DER_ETAB() {
        return HIST_LIBELLE_DER_ETAB;
    }

    public void setHIST_LIBELLE_DER_ETAB(String hIST_LIBELLE_DER_ETAB) {
        HIST_LIBELLE_DER_ETAB = hIST_LIBELLE_DER_ETAB;
    }

    public String getHIST_VILLE_DER_ETAB() {
        return HIST_VILLE_DER_ETAB;
    }

    public void setHIST_VILLE_DER_ETAB(String hIST_VILLE_DER_ETAB) {
        HIST_VILLE_DER_ETAB = hIST_VILLE_DER_ETAB;
    }

    public String getDPTG_CODE_DER_ETAB() {
        return DPTG_CODE_DER_ETAB;
    }

    public void setDPTG_CODE_DER_ETAB(String dPTG_CODE_DER_ETAB) {
        DPTG_CODE_DER_ETAB = dPTG_CODE_DER_ETAB;
    }

    public String getPAYS_CODE_DER_ETAB() {
        return PAYS_CODE_DER_ETAB;
    }

    public void setPAYS_CODE_DER_ETAB(String pAYS_CODE_DER_ETAB) {
        PAYS_CODE_DER_ETAB = pAYS_CODE_DER_ETAB;
    }

}
