package nc.unc.importparcoursup.dao.pgiCocktailDAO.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = Pays.TABLE_NAME)
public class Pays {
    public final static String TABLE_NAME = "GRHUM.PAYS";
    @Id
    @Column(name = "C_PAYS")
    private String codepays;

    public Pays() {
    }

    public Pays(String codepays) {
        this.codepays = codepays;
    }

    public String getCodepays() {
        return codepays;
    }

    public void setCodepays(String codepays) {
        this.codepays = codepays;
    }
}
