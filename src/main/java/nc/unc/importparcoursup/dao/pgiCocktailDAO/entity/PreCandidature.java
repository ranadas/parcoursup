package nc.unc.importparcoursup.dao.pgiCocktailDAO.entity;

import javax.persistence.*;

@Entity
@Table(name = PreCandidature.TABLE_NAME)
public class PreCandidature {
    public final static String TABLE_NAME = "GARNUCHE.PRE_CANDIDATURE";
    @Id
    private long CANU_KEY;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "CAND_KEY" )
    private PreCandidat preCandidat;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CAND_DSPE_CODE")
    private ScolFormationSpecialisation scolFormationSpecialisation;

    private long CAND_ANNEE_SUIVIE;
    private String RES_CODE;

    public long getCANU_KEY() {
        return CANU_KEY;
    }

    public void setCANU_KEY(long CANU_KEY) {
        this.CANU_KEY = CANU_KEY;
    }

    public PreCandidat getPreCandidat() {
        return preCandidat;
    }

    public void setPreCandidat(PreCandidat preCandidat) {
        this.preCandidat = preCandidat;
    }

    public ScolFormationSpecialisation getScolFormationSpecialisation() {
        return scolFormationSpecialisation;
    }

    public void setScolFormationSpecialisation(ScolFormationSpecialisation scolFormationSpecialisation) {
        this.scolFormationSpecialisation = scolFormationSpecialisation;
    }

    public long getCAND_ANNEE_SUIVIE() {
        return CAND_ANNEE_SUIVIE;
    }

    public void setCAND_ANNEE_SUIVIE(long CAND_ANNEE_SUIVIE) {
        this.CAND_ANNEE_SUIVIE = CAND_ANNEE_SUIVIE;
    }

    public String getRES_CODE() {
        return RES_CODE;
    }

    public void setRES_CODE(String RES_CODE) {
        this.RES_CODE = RES_CODE;
    }
}
