package nc.unc.importparcoursup.dao.pgiCocktailDAO.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = ScolFormationSpecialisation.TABLE_NAME)
public class ScolFormationSpecialisation {
    public final static String TABLE_NAME = "SCOLARITE.SCOL_FORMATION_SPECIALISATION";
    @Id
    @Column(name = "FSPN_KEY")
    private Long fspnkey;
    @Column(name = "FDIP_CODE")
    private String fdipcode;
    @Column(name = "FSPN_LIBELLE")
    private String FSPN_LIBELLE;
    @Column(name = "FSPN_ABREVIATION")
    private String FSPN_ABREVIATION;

    public String getFdipcode() {
        return fdipcode;
    }

    public void setFdipcode(String fdipcode) {
        this.fdipcode = fdipcode;
    }

    public Long getFspnkey() {
        return fspnkey;
    }

    public void setFspnkey(Long fspnkey) {
        this.fspnkey = fspnkey;
    }

    public String getFSPN_LIBELLE() {
        return FSPN_LIBELLE;
    }

    public void setFSPN_LIBELLE(String FSPN_LIBELLE) {
        this.FSPN_LIBELLE = FSPN_LIBELLE;
    }

    public String getFSPN_ABREVIATION() {
        return FSPN_ABREVIATION;
    }

    public void setFSPN_ABREVIATION(String FSPN_ABREVIATION) {
        this.FSPN_ABREVIATION = FSPN_ABREVIATION;
    }
}
