package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import org.springframework.data.repository.CrudRepository;

public interface PreCandidatRepository extends CrudRepository<PreCandidat, Long> {
}