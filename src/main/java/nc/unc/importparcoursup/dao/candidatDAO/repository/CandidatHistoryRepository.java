package nc.unc.importparcoursup.dao.candidatDAO.repository;

import nc.unc.importparcoursup.dao.IHistoryRepository;
import nc.unc.importparcoursup.dao.candidatDAO.CandidatHistory;
import nc.unc.importparcoursup.model.UAI;
import org.springframework.data.repository.CrudRepository;

public interface CandidatHistoryRepository extends CrudRepository<CandidatHistory, Long>, IHistoryRepository {
    CandidatHistory findFirstByFileCheckSum(String checkSum);

    CandidatHistory findTopByUaiOrderByJobExecutionDateDesc(UAI uai);

    CandidatHistory findTopByOrderByJobExecutionDateDesc();
}
