package nc.unc.importparcoursup.dao.candidatDAO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Objects;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "candidatEntityManagerFactory", transactionManagerRef = "candidatTransactionManager")
public class CandidatDAOConfig {

    @Value("${datasource.parcoursup-candidature.hibernate-hbm2ddl-auto}")
    private String ddlMode;

    @Value("${datasource.parcoursup-candidature.jdbc.driverClassName:#{null}}")
    private String driver;

    @Bean
    public PlatformTransactionManager candidatTransactionManager() {
        return new JpaTransactionManager(Objects.requireNonNull(candidatEntityManagerFactory().getObject()));
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean candidatEntityManagerFactory() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", ddlMode);
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL94Dialect");

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

        factoryBean.setDataSource(parcoursupCandidatDataSource());
        factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        factoryBean.setPackagesToScan(CandidatDAOConfig.class.getPackage().getName());
        factoryBean.setJpaPropertyMap(properties);

        return factoryBean;
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.parcoursup-candidature")
    public DataSource parcoursupCandidatDataSource() {
        if (driver != null) {
            return DataSourceBuilder.create()
                    .driverClassName(driver)
                    .build();
        }
        return DataSourceBuilder.create()
                .build();
    }
}