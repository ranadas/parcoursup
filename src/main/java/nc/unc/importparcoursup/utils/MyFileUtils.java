package nc.unc.importparcoursup.utils;

import com.opengamma.strata.collect.Unchecked;
import nc.unc.importparcoursup.model.UAI;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyFileUtils {
    private static final Logger log = LoggerFactory.getLogger(MyFileUtils.class);
    public static final String FILE_DATE_FORMAT = "yyyyMMdd_HHmm";
    public static final String FILE_FORMAT = FILE_DATE_FORMAT + "_UAI.csv";
    public static final String FILE_FORMAT_ERR = "Le nom du fichier ne correspond pas au format" + FILE_FORMAT;
    public static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final String FILE_FORMAT_REGEX = "\\d{8}_\\d{4}_(\\S{8})\\.csv";

    public static long getNbLinesByFile(File f) {
        return Unchecked.wrap(() -> {
            long nbLines = Files.lines(f.toPath(), Charset.defaultCharset()).count();
            return nbLines;
        });
    }

    public static boolean isEmpty(File f) {
        return MyFileUtils.readFile(f).trim().isEmpty();
    }

    public static String readFile(File f) {
        return Unchecked.wrap(() -> {
            return MapperUtils.removeUTF8BOM(FileUtils.readFileToString(f, UTF_8));
        });
    }

    // file format:
    public static UAI getUAIFromFileName(String fileName) {
        Pattern p = Pattern.compile(FILE_FORMAT_REGEX);
        Matcher m = p.matcher(fileName);
        if (m.matches()) {
            UAI ret = UAI.get(m.group(1));
            if (ret != null) {
                return ret;
            }
        }
        throw new RuntimeException(FILE_FORMAT_ERR + " - " + "Impossible d'extraire l'UAI : " + fileName);
    }

    public static Date getDateFromFileName(String fileName) {
        return Unchecked.wrap(() -> {
            SimpleDateFormat sdf = new SimpleDateFormat(FILE_DATE_FORMAT);
            sdf.setLenient(false);
            return sdf.parse(fileName);
        });
    }

    /**
     * @param fileName throw an exception if not valid
     */
    public static void checkFileName(String fileName) {
        getUAIFromFileName(fileName);
        getDateFromFileName(fileName);
    }

    public static String getMD5FromFile(File file) {
        return Unchecked.wrap(() -> {
            FileInputStream fis = new FileInputStream(file);
            String md5 = DigestUtils.md5Hex(fis);
            fis.close();
            return md5;
        });
    }
}
