package nc.unc.importparcoursup.utils;

import nc.unc.importparcoursup.dao.admisDAO.AdmisRejet;
import nc.unc.importparcoursup.model.AdmisSkipException;

import java.util.Calendar;

import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.NOVEMBER;
import static java.util.Calendar.OCTOBER;

public class ScolUtils {
    /**
     * @return From november to december -> current year +1 else current year
     */
    public static int getCurrentScolYear () {
        int month = Calendar.getInstance().get(Calendar.MONTH);
        if (   month == OCTOBER || month == NOVEMBER || month == DECEMBER) {
            return Calendar.getInstance().get(Calendar.YEAR) +1;
        }

        return Calendar.getInstance().get(Calendar.YEAR);
    }

    /**
     *
     * @param pays
     * @return 99100->100 et 100->100
     */
    public static String getPays (String pays) {
        if (pays !=null && pays.length() == 5) {
            return pays.substring(2);
        }
        return pays;
    }
}
