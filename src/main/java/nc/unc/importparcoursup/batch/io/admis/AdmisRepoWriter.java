package nc.unc.importparcoursup.batch.io.admis;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class AdmisRepoWriter implements ItemWriter<Admis> {
    private static final Logger log = LoggerFactory.getLogger(AdmisRepoWriter.class);

    private AdmisRepository admisRepo;

    public AdmisRepoWriter(AdmisRepository admisRepo) {
        this.admisRepo = admisRepo;
    }

    @Override
    public void write(List<? extends Admis> items) {
        log.debug("Received the information of {} admis", items.size());
        items.forEach(admis -> admisRepo.save(admis));
    }
}