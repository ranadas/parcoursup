package nc.unc.importparcoursup.batch.io.admis;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisPageableRepository;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.data.domain.Sort.Direction;

import java.util.HashMap;
import java.util.List;

public class AdmisRepoReader extends RepositoryItemReader<Admis> {

    private final AdmisPageableRepository repository;
    private AdmisHistory lastExecHistory;

    public AdmisRepoReader(AdmisPageableRepository repository, AdmisHistory lastExecHistory) {
        super();
        this.repository = repository;
        this.lastExecHistory = lastExecHistory;
        init();
    }

    protected void init() {
        this.setRepository(this.repository);

        // sort par defaut (obligatoire)
        this.setSort(new HashMap<String, Direction>());

        this.setMethodName("findAllAdmisByAdmisHistory");
        this.setArguments(List.of(lastExecHistory));
    }
}