package nc.unc.importparcoursup.batch.io.PreCandidat;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatPageableRepository;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.data.domain.Sort.Direction;

import java.util.HashMap;

public class PreCandidatRepoReader extends RepositoryItemReader<PreCandidat> {
    private final PreCandidatPageableRepository repository;

    public PreCandidatRepoReader(PreCandidatPageableRepository repository) {
        super();
        this.repository = repository;
        init();
    }

    protected void init() {
        this.setRepository(this.repository);
        // sort par defaut (obligatoire)
        this.setSort(new HashMap<String, Direction>());
        this.setMethodName("findAll");
    }
}