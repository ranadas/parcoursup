package nc.unc.importparcoursup.batch;

import nc.unc.importparcoursup.batch.io.PreCandidat.PreCandidatRepoReader;
import nc.unc.importparcoursup.batch.io.PreCandidat.PreCandidatRepoWriter;
import nc.unc.importparcoursup.batch.io.admis.AdmisFileReader;
import nc.unc.importparcoursup.batch.io.admis.AdmisRepoReader;
import nc.unc.importparcoursup.batch.io.admis.AdmisRepoWriter;
import nc.unc.importparcoursup.batch.mail.EmailService;
import nc.unc.importparcoursup.batch.processor.AdmisItemProcessor;
import nc.unc.importparcoursup.batch.processor.AdmisSkipListener;
import nc.unc.importparcoursup.batch.processor.PreCandidatItemProcessor;
import nc.unc.importparcoursup.batch.tasks.ArchiveFileTask;
import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisPageableRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.*;
import nc.unc.importparcoursup.model.AdmisSkipException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.IOException;

@Configuration
@EnableBatchProcessing
public class BatchAdmisConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private AdmisRepository admisRepository;
    @Autowired
    private AdmisHistoryRepository admisHistoryRepository;
    @Autowired
    AdmisPageableRepository admisPageableRepo;
    @Autowired
    PreCandidatRepository preCandidatRepository;
    @Autowired
    PreCandidatureRepository preCandidatureRepository;
    @Autowired
    PreCandidatPageableRepository preCandidatPageableRepository;
    @Autowired
    AdmisRejetRepository admisRejetRepository;
    @Autowired
    ScolFormationSpecialisationRepository scolFormationSpecialisationRepository;
    @Autowired
    BacRepository bacRepository;
    @Autowired
    PaysRepository paysRepository;
    @Autowired
    EmailService emailService;
    @Value("${file.local-tmp-file}")
    private String inputFile;
    @Value("${file.archive-directory}")
    private FileSystemResource archiveDirectory;

    @Bean
    @StepScope
    public AdmisRepoReader readerDBAdmis() {
        return new AdmisRepoReader(admisPageableRepo, admisHistoryRepository.findTopByOrderByJobExecutionDateDesc());
    }

    @Bean
    @StepScope
    public PreCandidatRepoReader readerDBPreCandidat() {
        return new PreCandidatRepoReader(preCandidatPageableRepository);
    }

    @Bean
    @StepScope
    public AdmisFileReader readerFileAdmis(@Value("#{jobParameters[fileName]}") String fileName,
                                           @Value("#{jobParameters[userLogin]}") String userLogin,
                                           @Value("#{jobParameters[fileCheckSum]}") String fileCheckSum,
                                           @Value("#{jobParameters[uploadedFileName]}") String uploadedFileName) throws IOException {
        AdmisHistory admisHistory = new AdmisHistory(userLogin, uploadedFileName, fileCheckSum);
        admisHistoryRepository.save(admisHistory);
        return new AdmisFileReader(new File(fileName), admisHistory);
    }

    @Bean
    @StepScope
    public AdmisRepoWriter writerDBAdmis() {
        return new AdmisRepoWriter(admisRepository);
    }

    @Bean
    @StepScope
    public PreCandidatRepoWriter writerPGICocktail() {
        return new PreCandidatRepoWriter(preCandidatRepository);
    }

    @Bean
    @StepScope
    public AdmisSkipListener AdmisSkipListener() {
        return new AdmisSkipListener(admisRejetRepository);
    }

    @Bean
    @StepScope
    public ArchiveFileTask archiveFileTask(@Value("#{jobParameters[fileName]}") String fileName) {
        return new ArchiveFileTask(new File(fileName), archiveDirectory);
    }

    @Bean
    @Qualifier("importAdmisJob")
    public Job importUserJob(Step step1_readFileWriteDB, Step step2_archiveFile, Step step3_readDBWriteDB, Step remove_preCandidat) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .start(step1_readFileWriteDB)
                .next(step2_archiveFile)
                .next(remove_preCandidat)
                .next(step3_readDBWriteDB)
                .build();
    }

    @Bean
    public Step step1_readFileWriteDB(AdmisRepository repository) throws Exception {
        return stepBuilderFactory.get("step1_readFileWriteDB")
                .<Admis, Admis>chunk(100)
                .reader(readerFileAdmis("noParamYet", "noParamYet", "noParamYet", "noParamYet"))
                .writer(writerDBAdmis()).build();
    }

    @Bean
    public Step step2_archiveFile() {
        return stepBuilderFactory.get("step2_archiveFile")
                .tasklet(archiveFileTask("noparamyet"))
                .build();
    }

    @Bean
    public Step step3_readDBWriteDB() {
        return stepBuilderFactory.get("step3_readDBWriteDB")
                .<Admis, PreCandidat>chunk(100)
                .reader(readerDBAdmis())
                .processor(new AdmisItemProcessor(preCandidatRepository, scolFormationSpecialisationRepository, bacRepository, paysRepository, admisRejetRepository, emailService))
                .faultTolerant()
                .skip(AdmisSkipException.class)
                .skipLimit(50000)
                .listener(AdmisSkipListener())
                .writer(writerPGICocktail()).build();
    }

    @Bean
    public Step remove_preCandidat() {
        return stepBuilderFactory.get("remove_preCandidat")
                .<PreCandidat, PreCandidat>chunk(100)
                .reader(readerDBPreCandidat())
                .processor(new PreCandidatItemProcessor(preCandidatRepository, admisRepository, admisHistoryRepository))
                .build();
    }
}
