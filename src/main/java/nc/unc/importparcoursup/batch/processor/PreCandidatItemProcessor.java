package nc.unc.importparcoursup.batch.processor;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatRepository;
import nc.unc.importparcoursup.model.UAI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PreCandidatItemProcessor implements ItemProcessor<PreCandidat, PreCandidat> {
    private static final Logger log = LoggerFactory.getLogger(PreCandidatItemProcessor.class);
    private PreCandidatRepository preCandidatRepository;
    private AdmisRepository admisRepository;
    private AdmisHistoryRepository admisHistoryRepository;
    private Collection<Admis> lastAdmisImported;

    public PreCandidatItemProcessor(PreCandidatRepository preCandidatRepository,
                                    AdmisRepository admisRepository,
                                    AdmisHistoryRepository admisHistoryRepository) {
        this.preCandidatRepository = preCandidatRepository;
        this.admisRepository = admisRepository;
        this.admisHistoryRepository = admisHistoryRepository;
    }

    @Override
    public PreCandidat process(PreCandidat preCandidat) {
        // is PreCandidat remove from parcoursup
        Stream<Admis> admisHistoryIUT = admisRepository.findAllAdmisByAdmisHistory(admisHistoryRepository.findTopByUaiOrderByJobExecutionDateDesc(UAI.IUT)).stream();
        Stream<Admis> admisHistoryUNIV = admisRepository.findAllAdmisByAdmisHistory(admisHistoryRepository.findTopByUaiOrderByJobExecutionDateDesc(UAI.IUT)).stream();
        Collection<Admis> lastAdmisImported = Stream.concat(admisHistoryIUT, admisHistoryUNIV).collect(Collectors.toList());
        boolean preCandidatIsInAdmis = lastAdmisImported.stream().anyMatch(admis -> isAdmisEqualsPreCandidat(preCandidat, admis));
        if (!preCandidatIsInAdmis) {
            preCandidatRepository.delete(preCandidat);
        }
        return null;
    }

    private boolean isAdmisEqualsPreCandidat(PreCandidat preCandidat, Admis admis) {
        return admis.getCandNumero() !=null && Long.parseLong(admis.getCandNumero()) == preCandidat.getCAND_KEY();
    }
}