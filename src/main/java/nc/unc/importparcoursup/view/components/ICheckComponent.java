package nc.unc.importparcoursup.view.components;

public interface ICheckComponent {
    boolean isValid();
}
