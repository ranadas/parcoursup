package nc.unc.importparcoursup.view.components;

import com.opengamma.strata.collect.Unchecked;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.UploadI18N;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.shared.Registration;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Optional;

public class UploadComponent extends Upload {
    private static final long serialVersionUID = 1L;
    private static File myFile;
    private String uploadedFileName;

    public UploadComponent(String filePath) {
        super();

        MemoryBuffer buffer = new MemoryBuffer();
        setReceiver(buffer);
        addSucceededListener(event -> copyfile(filePath, buffer));

        // traduction
        UploadI18N i18n = new UploadI18N();
        i18n.setDropFiles(
                new UploadI18N.DropFiles()
                        .setOne("ou déposez le fichier ici"))
                .setAddFiles(new UploadI18N.AddFiles().setOne("Sélectionnez le fichier"))
                .setUploading(new UploadI18N.Uploading()
                        .setStatus(new UploadI18N.Uploading.Status().setConnecting("Connexion...")
                                .setStalled("Bloqué.").setProcessing("En cours..."))

                );
        this.setI18n(i18n);

    }

    private void copyfile(String filePath, MemoryBuffer buffer) {
        this.uploadedFileName = buffer.getFileName();
        Unchecked.wrap(() -> {
            myFile = new File(filePath);
            FileUtils.copyInputStreamToFile(buffer.getInputStream(), myFile);
        });
    }

    public Optional<File> getFile() {
        return Optional.ofNullable(myFile);
    }

    public Registration addFileRemoveListener(ComponentEventListener<FileRemoveEvent> listener) {
        return super.addListener(FileRemoveEvent.class, listener);
    }

    public String getUploadedFileName() {
        return uploadedFileName;
    }

    @DomEvent("file-remove")
    public static class FileRemoveEvent extends ComponentEvent<Upload> {
        private static final long serialVersionUID = 1L;

        public FileRemoveEvent(Upload source, boolean fromClient) {
            super(source, fromClient);
            myFile = null;
        }
    }

}