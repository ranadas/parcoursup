package nc.unc.importparcoursup.view.components;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import nc.unc.importparcoursup.dao.candidatDAO.repository.CandidatHistoryRepository;
import nc.unc.importparcoursup.view.verificationrules.CheckElement;
import nc.unc.importparcoursup.view.verificationrules.FileIsNotEmpty;
import nc.unc.importparcoursup.view.verificationrules.FileNameIsCorrect;
import nc.unc.importparcoursup.view.verificationrules.FileNotAlreadyUploaded;

import java.util.List;

public class CheckComponentCandidat extends VerticalLayout implements ICheckComponent {
    private static final long serialVersionUID = 1L;

    CandidatHistoryRepository candidatRepo;
    UploadComponent myUploadComponent;
    List<CheckElement> checks;

    public CheckComponentCandidat(CandidatHistoryRepository candidatRepo, UploadComponent myUploadComponent) {
        this.candidatRepo = candidatRepo;
        this.myUploadComponent = myUploadComponent;

        checks = List.of(new FileIsNotEmpty(myUploadComponent),
                new FileNameIsCorrect(myUploadComponent),
                new FileNotAlreadyUploaded(myUploadComponent, candidatRepo));
        checks.forEach(this::addComponent);

    }

    private void addComponent(CheckElement element) {
        element.setSizeFull();
        add(element);
    }

    public final boolean isValid() {
        return checks.stream().allMatch(elem -> elem.isValid());
    }

    public void update() {
        checks.forEach(CheckElement::update);
    }
}
