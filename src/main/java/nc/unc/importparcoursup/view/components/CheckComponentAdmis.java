package nc.unc.importparcoursup.view.components;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import nc.unc.importparcoursup.view.verificationrules.*;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class CheckComponentAdmis extends VerticalLayout implements ICheckComponent {
    private static final long serialVersionUID = 1L;
    AdmisRepository admisRepo;
    AdmisHistoryRepository admisHistoryRepository;
    UploadComponent myUploadComponent;
    List<CheckElement> checks;

    public CheckComponentAdmis(AdmisRepository admisRepo, AdmisHistoryRepository admisHistoryRepository,
                               UploadComponent myUploadComponent) {
        this.admisRepo = admisRepo;
        this.admisHistoryRepository = admisHistoryRepository;
        this.myUploadComponent = myUploadComponent;
        checks = List.of(new FileIsNotEmpty(myUploadComponent));
        checks = List.of(new FileIsNotEmpty(myUploadComponent),
                new FileNameIsCorrect(myUploadComponent),
                new FileDateBeforeToday(myUploadComponent),
                new FileNotAlreadyUploaded(myUploadComponent, admisHistoryRepository),
                new DateAfterLastUploadForSameUAI(myUploadComponent, admisHistoryRepository));
        checks.stream().forEach(this::addComponent);
    }

    private void addComponent(CheckElement element) {
        element.setSizeFull();
        add(element);
    }

    public final boolean isValid() {
        return checks.stream().allMatch(elem -> elem.isValid());
    }

    public void update() {
        if (myUploadComponent.getFile().isPresent() && StringUtils.isNoneEmpty(myUploadComponent.getUploadedFileName())) {
            checks.forEach(CheckElement::update);
        }
    }
}
