package nc.unc.importparcoursup.view;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import nc.unc.importparcoursup.Application;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import nc.unc.importparcoursup.view.components.*;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

@Route(layout = TopBarLayout.class, value = "secured/admissions")
@PageTitle(Application.APP_NAME)
public class AdmisMainView extends HorizontalLayout {
    static final long serialVersionUID = 1L;
    private ImportButtonComponent goButton;
    private UploadComponent myUploadComponent;
    private InformationComponent infoComponent;
    private CheckComponentAdmis checkComponent;
    private Stepper stepper;

    public AdmisMainView(@Autowired AdmisHistoryRepository admisHistoryRepository,
                         @Autowired AdmisRepository admisRepo,
                         @Autowired AdmisRejetRepository admisRejetRepository,
                         @Autowired @Qualifier("importAdmisJob") Job jobImportCsv,
                         @Autowired JobLauncher jobLauncher,
                         @Value("${file.local-tmp-file}") String inputFile) {
        VerticalLayout leftContent = new VerticalLayout();
        VerticalLayout rightContent = new VerticalLayout();
        // init
        stepper = new Stepper("Selection du fichier", "Vérifications", "Importation des données");
        myUploadComponent = buildUploadComponent(admisRepo, jobImportCsv, jobLauncher, inputFile);
        infoComponent = new InformationComponent(admisHistoryRepository, myUploadComponent, admisRepo, admisRejetRepository);
        checkComponent = new CheckComponentAdmis(admisRepo, admisHistoryRepository, myUploadComponent);
        goButton = new ImportButtonComponent(jobImportCsv, jobLauncher, myUploadComponent, stepper, checkComponent, infoComponent);
        // RIGHT
        rightContent.add(infoComponent);
        // LEFT
        // upload part
        leftContent.add(stepper.getStepComponent(Stepper.Step.STEP_ONE));
        leftContent.add(myUploadComponent);
        // vérification part
        leftContent.add(stepper.getStepComponent(Stepper.Step.STEP_TWO));
        leftContent.add(checkComponent);
        // Button part
        leftContent.add(stepper.getStepComponent(Stepper.Step.STEP_THREE));
        leftContent.add(goButton);
        add(leftContent, infoComponent);
        // update data
        updateData();
    }

    private UploadComponent buildUploadComponent(AdmisRepository admisRepo, Job jobImportCsv, JobLauncher jobLauncher, String inputFile) {
        myUploadComponent = new UploadComponent(inputFile);
        myUploadComponent.addSucceededListener(event -> updateData());
        myUploadComponent.addFileRemoveListener(e -> updateData());
        return myUploadComponent;
    }

    private void updateData() {
        infoComponent.update();
        goButton.update();
        checkComponent.update();
        stepper.setStep(Stepper.Step.STEP_NONE);
        if (myUploadComponent.getFile().isPresent()) {
            stepper.setStep(Stepper.Step.STEP_ONE);
            if (checkComponent.isValid()) {
                stepper.setStep(Stepper.Step.STEP_TWO);
            }
        }
    }
}
