/*
 * Copyright 2000-2017 Vaadin Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package nc.unc.importparcoursup.view;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import nc.unc.importparcoursup.Application;
import org.springframework.security.core.context.SecurityContextHolder;

@Push
@StyleSheet("frontend://styles.css")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
public class TopBarLayout extends Div implements RouterLayout {
    private static final long serialVersionUID = 1L;

    public TopBarLayout() {

        // Title
        H2 title = new H2(Application.APP_NAME);
        title.addClassName("main-layout__title");

        // Logout
        Button myBtn = new Button(extractloginName(), new Icon(VaadinIcon.SIGN_OUT), e -> logout());

        // Navigation

        RouterLink candidatures = new RouterLink(null, CandidatMainView.class);
        candidatures.add(new Icon(VaadinIcon.USER_CLOCK), new Text("Candidatures"));
        candidatures.addClassName("main-layout__nav-item");

        RouterLink admission = new RouterLink(null, AdmisMainView.class);
        admission.add(new Icon(VaadinIcon.USER_CHECK), new Text("Admissions"));
        admission.addClassName("main-layout__nav-item");

        RouterLink rejet = new RouterLink(null, InformationMainView.class);
        rejet.add(new Icon(VaadinIcon.INFO_CIRCLE), new Text("Informations"));
        rejet.addClassName("main-layout__nav-item");

        // View
        Div navigation = new Div(new Div(candidatures), new Div(admission), new Div(rejet));
        navigation.addClassName("main-layout__nav");

        Div header = new Div(title, navigation, new Div(myBtn));
        header.addClassName("main-layout__header");



        add(header);

        addClassName("main-layout");
    }

    private void logout() {
        UI.getCurrent().getPage().executeJavaScript("window.open(\"logout/cas\", \"_self\");");
    }

    public String extractloginName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
