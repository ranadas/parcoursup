package nc.unc.importparcoursup.view.verificationrules;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import nc.unc.importparcoursup.dao.IHistoryRepository;
import nc.unc.importparcoursup.utils.ViewUtils;
import nc.unc.importparcoursup.view.components.UploadComponent;

public abstract class CheckElement extends FlexLayout {
    private static final long serialVersionUID = 1L;

    protected Label valueLbl = new Label();
    protected UploadComponent uploadComponent;
    protected IHistoryRepository historyRepository;

    public CheckElement(UploadComponent upload, IHistoryRepository historyRepository) {
        super();
        VerticalLayout componentLeft = new VerticalLayout();
        VerticalLayout componentRight = new VerticalLayout();
        componentLeft.add(new Label(getLabel()));
        componentRight.add(valueLbl);
        add(componentLeft, componentRight);

        this.uploadComponent = upload;
        this.historyRepository = historyRepository;
    }

    public CheckElement(UploadComponent upload) {
        this(upload, null);
    }

    public final void update() {
        if (isValid()) {
            ViewUtils.checkedAndGreen(valueLbl);
        } else {
            ViewUtils.uncheckedAndRed(valueLbl);
        }
    }

    String getFileName() {
        return uploadComponent.getUploadedFileName();
    }

    private boolean isInvalidCommon() {
        boolean isFileUploaded = uploadComponent.getFile().isPresent();
        return !isFileUploaded;
    }

    public final boolean isValid() {
        return !isInvalidCommon() && isValidCheck();
    }

    public abstract boolean isValidCheck();

    protected abstract String getLabel();
}
