package nc.unc.importparcoursup.view.informationrules;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opengamma.strata.collect.Unchecked;
import nc.unc.importparcoursup.batch.io.admis.AdmisFileReader;

import java.io.File;
import java.io.FileReader;

public class NbColumnInFileIE extends InformationElement<File> {
    private static final long serialVersionUID = 1L;

    @Override
    public String updateValue(File item) {
        return Unchecked.wrap(() -> {
            CSVReaderBuilder builder;
            builder = new CSVReaderBuilder(new FileReader(item));
            CSVParser parser = new CSVParserBuilder().withSeparator(AdmisFileReader.SEPARATOR).build();
            CSVReader reader = builder.withCSVParser(parser).build();
            return "" + reader.readNext().length;
        });
    }

    @Override
    protected String getLabel() {
        return "Fichier - Colonnes";
    }
}
