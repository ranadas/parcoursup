package nc.unc.importparcoursup.view.informationrules;

import nc.unc.importparcoursup.utils.MyFileUtils;

import java.io.File;

public class NbLinesInFileIE extends InformationElement<File> {
    private static final long serialVersionUID = 1L;

    @Override
    public String updateValue(File item) {
        return "" + MyFileUtils.getNbLinesByFile(item);
    }

    @Override
    protected String getLabel() {
        return "Fichier - Lignes";
    }

}
