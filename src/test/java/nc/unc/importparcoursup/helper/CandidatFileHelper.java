package nc.unc.importparcoursup.helper;

public class CandidatFileHelper extends FileHelper {

    private static final String LINE_HEADER = "\uFEFFGroupe;Classement;Code aménagement (oui - si);Numéro;Code formation; Formation; Filière;Nom;Prénom;Libelle groupe;Deuxième prénom;Troisième prénom;Civilité;Sexe;Date de naissance;Ville de naissance (Code);Ville de naissance;Département de naissance;Pays de naissance (Code);Pays de naissance;Nationalité;Boursier des lycées;Boursier des lycées (code);Boursier (Code);Boursier;Boursier certifié;Boursier certifié (Code);Nombre de parts ou échelon;ACB (Code);ACB;Profil du candidat;Profil du candidat (Code);Type établissement;Type établissement (Code);Contrat établissement;Contrat établissement (Code);UAI établissement;Libellé établissement;Commune établissement;Département établissement;Pays établissement;Pays établissement (Code);Téléphone établissement;Fax établissement;Niveau d'étude actuelle;Niveau d'étude actuelle;Numéro INE;Numéro CEF;Type de formation (Code);Type de formation;Série/Domaine/Filière;Classe;Option internationale (Code);Option internationale;Dominante;Dominante (Code);Spécialité/Mention/Voie;Spécialité (Code);LV 1 scolarité;LV 1 (Code);LV 2 scolarité;LV 2 (Code);LV 3 scolarité;LV 3 (Code);Option 1;Option 1 (Code);Option 2;Option 2 (Code);Diplôme;Diplôme (Code);Série diplôme (Code);Série diplôme;Dominante diplôme;Dominante diplôme (Code);Spécialité diplôme;Spécialité diplôme (Code);Mention diplôme;Mention diplôme (Code);Mois et année d'obtention du bac;Académie du bac;Académie du bac (Code);LV 1 Bac;LV1 Bac (Code);LV 2 Bac;LV2 Bac (Code);Option du bac (Code);Option du bac;Diplôme étranger (Code);Diplôme étranger;Test de français;Test de français (Code);Niveau de français;Date test de français;Aménagement handicap (code);Aménagement handicap;Date de validation;Candidature validée (Code);Candidature validée (O/N);Sportif de haut niveau (code);Sportif de haut niveau;Artiste de haut niveau;Artiste de haut niveau (code);Date inscription;Date d'impression;Fiche imprimée (O/N);Fiche imprimée (Code);Eligible à la politique de la ville;Fiche de réorientation;Fiche de réorientation (code);Dossier papier;Dossier papier (code);Année d'entrée en seconde;Dossier dématérialisé (code);Dossier dématérialisé;";
    public static final String LINE_1 = "104;ECF;;70612;104;DUT - Production;Métiers du multimédia et de l'internet;TAUGAMOA;Malekalita;Tous les candidats;Sandra;;Mme;F;29/01/1997;98818;Nouméa;;99100;Nouvelle-Calédonie;FR;Non;0;0;Non boursier;;;;1;Echelon 0 bis;Scolarisé dans le supérieur en France;11;Université;4;Public;1;0171463Y;Université de La Rochelle;La Rochelle;17;France;99100;05.46.45.91.14;05.46.44.93.76;3ème année d'études supérieures;13;4007000175Y;;82;Licences;Licence - Sciences - technologies - santé;;;;;;Génie civil;;;;;;;;;;;;Bac français obtenu;1;STI2D;Sciences et Technologies de l'Industrie et du Développement Durable;;;Architecture et construction;337;Admis sans mention;S;12/2014;Nouvelle-Calédonie;36;Anglais;2;Espagnol;4;0;MEF National;;;;;;;0;Non;29/09/2018;1;Oui;0;Non;Non;0;26/09/2018;;Non;0;Non;Non renseignée;2;Non;0;;1;Oui;";
    public static final String LINE_2 = "103;ENCF;;51133;103;DUT - Service;Gestion des entreprises et des administrations;GNAHOU;Ashley;Tous les candidats;Marie-Pierre;Kamea;Mme;F;23/11/1998;98829;Thio;;99100;Nouvelle-Calédonie;FR;Non;0;1;Boursier du secondaire;Non;0;;2;Echelon 1;En Terminale;1;Lycée à classe postbac;1;Privé sous contrat d'association;2;9830272D;Lycée professionnel François d'Assise (DDEC);Bourail;988;France;99100;44.13.76;44.15.05;Terminale;10;133272248CK;;10;Terminale;P;TBPGA1;0;;;;Gestion-Administration;348;Anglais;2;Espagnol;4;;;;;;;Bac en préparation;4;P;Professionnelle;;;Gestion-Administration;348;;;;Nouvelle-Calédonie;36;Anglais;2;Espagnol;4;0;MEF National;;;;;;;0;Non;;0;Non;0;Non;Non;0;06/09/2018;;Non;0;Non;Non renseignée;;Non;0;2014;1;Oui;";

    @Override
    protected String getOneLineFileContent() {
        return LINE_HEADER;
    }

    @Override
    protected Long getdeFaultId() {
        return null;
    }

    @Override
    protected String getFileName() {
        return "20181008_0718_9830445S.csv";
    }
}