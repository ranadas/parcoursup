package nc.unc.importparcoursup.helper;

import com.opengamma.strata.collect.Unchecked;
import nc.unc.importparcoursup.utils.MyFileUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;

public abstract class FileHelper {
    public static final String TMP_DIR = "target/TMP_DIR_FOR_ArchiveFileTaskTest";

    private File file;

    protected abstract String getOneLineFileContent();

    protected abstract Long getdeFaultId();

    protected abstract String getFileName();

    FileHelper() {
        clearTmpRepository();
    }

    private void clearTmpRepository() {
        Unchecked.wrap(() -> {
            FileUtils.deleteQuietly(new File(TMP_DIR));
        });
    }

    public FileHelper createOneLineFile() {
        clearTmpRepository();
        createEmptyFile();
        return addLineToFile(getOneLineFileContent());
    }

    public FileHelper createOneLineFileWithId(Long id) {
        clearTmpRepository();
        createEmptyFile();
        return addLineToFile(id + getOneLineFileContent());
    }

    public FileHelper createEmptyFile() {
        clearTmpRepository();
        this.file = new File(TMP_DIR + "/tmpIn/" + getFileName());
        Unchecked.wrap(() -> {
            FileUtils.writeStringToFile(file, "", MyFileUtils.UTF_8, false);
        });
        return this;
    }

    public FileHelper addLineToFile(String s) {
        Unchecked.wrap(() -> {
            if (!MyFileUtils.isEmpty(file)) {
                FileUtils.writeStringToFile(file, "\n" + s, MyFileUtils.UTF_8, true);
            } else {
                FileUtils.writeStringToFile(file, s, MyFileUtils.UTF_8, false);
            }
        });
        return this;
    }

    public FileHelper addLineToFileWithId(Long id) {
        return addLineToFile(id + getOneLineFileContent());
    }

    public File getFile() {
        return file;
    }


}
