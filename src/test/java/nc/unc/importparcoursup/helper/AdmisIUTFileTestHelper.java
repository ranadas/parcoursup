package nc.unc.importparcoursup.helper;

import nc.unc.importparcoursup.model.UAI;

public class AdmisIUTFileTestHelper extends FileHelper {

    public static final Long DEFAULT_ID = 123456L;
    private static final UAI uai = UAI.IUT;
    public static final String LINE_CONTENT = "|" + "123" + "|" + "Mme" + "|TOTO|TOTO|TOTO|01011950|Saint-Jean-de-Braye|45284|45|100|100|TOTO|TOTO|17380|Tonnay-Boutonne|17448|100|06.|06.|toto@gmail.fr|S|2017|0620042J|Lycée Andre Malraux|Béthune|62|100||0333358W|I.U.T. de Bordeaux - Site Bordeaux-Bastide|Bordeaux|33|100|" + uai.getName() + "|2100270;524";


    @Override
    protected String getOneLineFileContent() {
        return LINE_CONTENT;
    }

    @Override
    protected Long getdeFaultId() {
        return DEFAULT_ID;
    }

    @Override
    protected String getFileName() {
        return "20181008_0718_" + uai.getName() + ".csv";
    }
}
