package nc.unc.importparcoursup.helper;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;

import java.io.File;

@Configuration
public class JobHelper {

    public static void launchJob(JobRepository jobRepo, Job job, File file) throws JobExecutionAlreadyRunningException, JobRestartException,
            JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        JobParameters jobParams = new JobParametersBuilder().addLong("uniqueness", System.nanoTime())
                .addString("fileName", file.getAbsolutePath()).addString("userLogin", "testlogin")
                .addString("uploadedFileName", file.getName()).addString("fileCheckSum", "testchecksum")
                .toJobParameters();

        SimpleJobLauncher sjl = new SimpleJobLauncher();
        sjl.setTaskExecutor(new SyncTaskExecutor());
        sjl.setJobRepository(jobRepo);
        sjl.run(job, jobParams);
    }

}
