package nc.unc.importparcoursup.io;

import nc.unc.importparcoursup.batch.io.admis.AdmisFileReader;
import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.helper.AdmisIUTFileTestHelper;
import nc.unc.importparcoursup.helper.ModelTestHelper;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AdmisFileReaderTest {

    @Test
    public void testRead() throws Exception {
        Long histoId = 1234L;
        ModelTestHelper modelHelper = new ModelTestHelper();

        // Création du reader
        AdmisFileReader reader = new AdmisFileReader(new AdmisIUTFileTestHelper().createOneLineFileWithId(AdmisIUTFileTestHelper.DEFAULT_ID).getFile(), modelHelper.createAdmisHistory(histoId, modelHelper.today()));
        Admis resultAdmis = reader.read();
        reader.read();

        // Controles
        assertThat(resultAdmis.getCandNumero()).isEqualTo("" + AdmisIUTFileTestHelper.DEFAULT_ID);
        assertThat(resultAdmis.getAdmisHistory().getId()).isEqualTo(histoId);
    }
}
