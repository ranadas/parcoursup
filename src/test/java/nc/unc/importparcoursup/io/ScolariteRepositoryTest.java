package nc.unc.importparcoursup.io;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationDiplome;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationHabilitation;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationSpecialisation;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.ScolFormationDiplomeRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.ScolFormationHabilitationRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.ScolFormationSpecialisationRepository;
import nc.unc.importparcoursup.utils.ScolUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ScolariteRepositoryTest {
    @Autowired
    ScolFormationSpecialisationRepository scolFormationSpecialisationRepository;
    @Autowired
    ScolFormationDiplomeRepository scolFormationDiplomeRepository;
    @Autowired
    ScolFormationHabilitationRepository scolFormationHabilitationRepository;


    @Test
    public void scolFormationSpecialisationRepositoryTestOuvert() {
        String fdipcode = createDiplome("O");
        Optional<ScolFormationSpecialisation> rez = scolFormationSpecialisationRepository.findByFdipCode(fdipcode, ScolUtils.getCurrentScolYear());
        assertThat(rez.get().getFdipcode()).isEqualTo(fdipcode);
    }

    @Test
    public void scolFormationSpecialisationRepositoryTestFerme() {
        String fdipcode = createDiplome("N");
        Optional<ScolFormationSpecialisation> rez = scolFormationSpecialisationRepository.findByFdipCode(fdipcode, ScolUtils.getCurrentScolYear());
        assertThat(rez).isEmpty();
    }

    private String createDiplome(String habOuvert) {
        String fdipcode = "111";
        Long fspnkey = 11L;
        ScolFormationSpecialisation sfs = new ScolFormationSpecialisation();
        sfs.setFspnkey(fspnkey);
        sfs.setFdipcode(fdipcode);
        ScolFormationDiplome sfd = new ScolFormationDiplome();
        sfd.setFdipcode(fdipcode);
        ScolFormationHabilitation sfh = new ScolFormationHabilitation();
        sfh.setFannkey(ScolUtils.getCurrentScolYear());
        sfh.setFHAB_KEY(12L);
        sfh.setFspnkey(fspnkey);
        sfh.setFhabniveau(1);
        sfh.setFhabouvert(habOuvert);
        scolFormationSpecialisationRepository.save(sfs);
        scolFormationDiplomeRepository.save(sfd);
        scolFormationHabilitationRepository.save(sfh);
        return fdipcode;
    }
}


